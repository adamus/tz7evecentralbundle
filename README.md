TZ-7: EveCentralBundle
======

This Symfony 2 bundle is part of the [TZ-7 project](https://bitbucket.org/adamus/tz-7)

* Requires: [Tz7\EveApiBundle](https://bitbucket.org/adamus/tz7eveapibundle)
