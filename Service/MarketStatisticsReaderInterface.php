<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Service;


use Tz7\EveCentralBundle\Model\ItemMarketStatistics;


interface MarketStatisticsReaderInterface
{
    /**
     * @param string $itemName
     * @param string[] $systemNames
     * @return ItemMarketStatistics[]
     */
    public function getItemStatisticsByName($itemName, array $systemNames = []);

    /**
     * @param $itemId
     * @param int[] $systemIds
     * @return ItemMarketStatistics[]
     */
    public function getItemStatisticsById($itemId, array $systemIds = []);
}