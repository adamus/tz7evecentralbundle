<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Service;


use Tz7\EveApiBundle\Service\TypeNameResolverInterface;
use Tz7\EveCentralBundle\Model\ItemMarketStatistics;


class MarketStatisticsReader implements MarketStatisticsReaderInterface
{
    /**
     * @var DataFetchServiceInterface
     */
    protected $dataFetcher;

    /**
     * @var TypeNameResolverInterface
     */
    protected $typeNameResolver;

    /**
     * @param DataFetchServiceInterface $dataFetcher
     * @param TypeNameResolverInterface $typeNameResolver
     */
    public function __construct(DataFetchServiceInterface $dataFetcher, TypeNameResolverInterface $typeNameResolver)
    {
        $this->dataFetcher = $dataFetcher;
        $this->typeNameResolver = $typeNameResolver;
    }

    /**
     * @param string $itemName
     * @param string[] $systemNames
     * @return ItemMarketStatistics[]
     */
    public function getItemStatisticsByName($itemName, array $systemNames = [])
    {
        $itemId = $this->typeNameResolver->getItemId($itemName);

        if (empty($systemNames)) {
            return $this->getItemStatisticsById($itemId);
        }

        $systemIds = array_map(
            function ($name) {
                return $this->typeNameResolver->getEntityId($name);
            },
            $systemNames
        );

        return array_combine($systemNames, $this->getItemStatisticsById($itemId, $systemIds));
    }

    /**
     * @param $itemId
     * @param int[] $systemIds
     * @return ItemMarketStatistics[]
     */
    public function getItemStatisticsById($itemId, array $systemIds = [])
    {
        if (empty($systemIds)) {
            return [0 => $this->dataFetcher->fetchItemMarketStats($itemId)];
        }

        $marketStatistics = array_flip($systemIds);
        array_walk(
            $marketStatistics,
            function (&$value, $systemId) use ($itemId) {
                $value = $this->dataFetcher->fetchItemMarketStats($itemId, $systemId);
            }
        );

        return $marketStatistics;
    }
}
