<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Service;


use GuzzleHttp\Client;
use Doctrine\Common\Cache\CacheProvider;
use Tz7\EveCentralBundle\Model\ItemMarketStatistics;
use Tz7\EveCentralBundle\Model\MarketStatistics;


class DataFetchService implements DataFetchServiceInterface
{
    const CACHE_LIFETIME = 3600;

    /**
     * @var CacheProvider
     */
    protected $cacheProvider;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @param CacheProvider $cacheProvider
     * @param string $base
     * @param string $userAgent
     */
    public function __construct(CacheProvider $cacheProvider, $base, $userAgent)
    {
        $this->cacheProvider = $cacheProvider;
        $this->client = new Client([
            'base_uri' => $base,
            'headers' => [
                'User-Agent' => $userAgent
            ]
        ]);
    }

    /**
     * @param int|string $typeId
     * @param int $systemId
     * @return ItemMarketStatistics
     */
    public function fetchItemMarketStats($typeId, $systemId = null)
    {
        $params = ['typeid' => $typeId];

        if ($systemId) {
            $params['usesystem'] = $systemId;
        }

        $xml = new \SimpleXmlElement((string)$this->fetch('api/marketstat', $params));
        $stat = $xml->marketstat->type;

        $buy = $this->buildMarketStatistics($stat->buy);
        $sell = $this->buildMarketStatistics($stat->sell);
        $all = $this->buildMarketStatistics($stat->all);

        return new ItemMarketStatistics($typeId, $buy, $sell, $all);
    }

    /**
     * @param string $function
     * @param array $params
     * @return string
     */
    protected function fetch($function, array $params = [])
    {
        $uri = $function . '?' . http_build_query($params);

        if (!($result = $this->cacheProvider->fetch($uri)))
        {
            $result = (string) $this->client->get($uri)->getBody();
            $this->cacheProvider->save($uri, $result, static::CACHE_LIFETIME);
        }

        return $result;
    }

    /**
     * @param \SimpleXMLElement $node
     * @return MarketStatistics
     */
    protected function buildMarketStatistics(\SimpleXMLElement $node)
    {
        return new MarketStatistics(
            (int) $node->volume,
            (float) $node->avg,
            (float) $node->min,
            (float) $node->max
        );
    }
}
