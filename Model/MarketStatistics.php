<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Model;


class MarketStatistics
{
    /** @var int */
    protected $volume;
    /** @var float */
    protected $avg;
    /** @var float */
    protected $min;
    /** @var float */
    protected $max;

    /**
     * @param int $volume
     * @param float $avg
     * @param float $min
     * @param float $max
     */
    public function __construct($volume, $avg, $min, $max)
    {
        $this->volume = $volume;
        $this->avg = $avg;
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @return float
     */
    public function getAvg()
    {
        return $this->avg;
    }

    /**
     * @return float
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return float
     */
    public function getMax()
    {
        return $this->max;
    }
}