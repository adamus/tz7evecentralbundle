<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Model;


class ItemMarketStatistics
{
    /** @var int */
    protected $itemId;
    /** @var MarketStatistics */
    protected $buy;
    /** @var MarketStatistics */
    protected $sell;
    /** @var MarketStatistics */
    protected $all;

    public function __construct($itemId, MarketStatistics $buy, MarketStatistics $sell, MarketStatistics $all)
    {
        $this->itemId = $itemId;
        $this->buy = $buy;
        $this->sell = $sell;
        $this->all = $all;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @return MarketStatistics
     */
    public function getBuy()
    {
        return $this->buy;
    }

    /**
     * @return MarketStatistics
     */
    public function getSell()
    {
        return $this->sell;
    }

    /**
     * @return MarketStatistics
     */
    public function getAll()
    {
        return $this->all;
    }
}