<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Tests\Service;

use Tz7\EveCentralBundle\Model\ItemMarketStatistics;
use Tz7\EveCentralBundle\Model\MarketStatistics;
use Tz7\EveCentralBundle\Service\DataFetchServiceInterface;
use Tz7\EveCentralBundle\Tests\KernelTestCase;


class DataFetchServiceTest extends KernelTestCase
{
    public function testFetchingItemMarketStats()
    {
        /** @var DataFetchServiceInterface $fetcher */
        $fetcher = $this->getContainer()->get('tz7.eve_central.data_fetch');
        $this->assertInstanceOf(DataFetchServiceInterface::class, $fetcher);

        $stat = $fetcher->fetchItemMarketStats(32226);
        $this->assertInstanceOf(ItemMarketStatistics::class, $stat);

        /** @var MarketStatistics $case */
        foreach ([$stat->getAll(), $stat->getBuy(), $stat->getSell()] as $case)
        {
            $this->assertInstanceOf(MarketStatistics::class, $case);
            $this->assertGreaterThan(0, $case->getVolume());
            $this->assertGreaterThan(0, $case->getAvg());
            $this->assertGreaterThan(0, $case->getMin());
            $this->assertGreaterThan(0, $case->getMax());
        }
    }
}
