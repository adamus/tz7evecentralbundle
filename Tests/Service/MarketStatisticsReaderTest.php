<?php

/*
 * This file is part of the Tz7\EveCentralBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCentralBundle\Tests\Service;

use Tz7\EveCentralBundle\Model\ItemMarketStatistics;
use Tz7\EveCentralBundle\Model\MarketStatistics;
use Tz7\EveCentralBundle\Service\MarketStatisticsReaderInterface;
use Tz7\EveCentralBundle\Tests\KernelTestCase;


class MarketStatisticsReaderTest extends KernelTestCase
{
    public function testMarketStatisticsById()
    {
        /** @var MarketStatisticsReaderInterface $reader */
        $reader = $this->getContainer()->get('tz7.eve_central.market_statistics_reader');
        $this->assertInstanceOf(MarketStatisticsReaderInterface::class, $reader);

        $systems = [30000142];

        $this->assertItemMarketStatistics($reader->getItemStatisticsById(32226, $systems), $systems);
    }

    public function testMarketStatisticsByName()
    {
        /** @var MarketStatisticsReaderInterface $reader */
        $reader = $this->getContainer()->get('tz7.eve_central.market_statistics_reader');
        $this->assertInstanceOf(MarketStatisticsReaderInterface::class, $reader);

        $this->assertItemMarketStatistics($reader->getItemStatisticsByName('Sabre', ['Jita']));
    }

    /**
     * @param ItemMarketStatistics[] $stats
     * @param array $systems
     */
    protected function assertItemMarketStatistics(array $stats, array $systems = [])
    {
        $index = 0;
        foreach ($stats as $system => $stat)
        {
            if ($system > 0) {
                $this->assertEquals($system, $systems[$index]);
                $index++;
            }

            $this->assertInstanceOf(ItemMarketStatistics::class, $stat);

            /** @var MarketStatistics $case */
            foreach ([$stat->getAll(), $stat->getBuy(), $stat->getSell()] as $case)
            {
                $this->assertInstanceOf(MarketStatistics::class, $case);
                $this->assertGreaterThan(0, $case->getVolume());
                $this->assertGreaterThan(0, $case->getAvg());
                $this->assertGreaterThan(0, $case->getMin());
                $this->assertGreaterThan(0, $case->getMax());
            }
        }
    }
}
